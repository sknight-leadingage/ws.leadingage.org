﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Security.Cryptography
Imports AAHSA.MDC_Framework.SystemFramework
Imports SalesForceClassLibrary
Imports SalesForceClassLibrary.SalesforceEnterprise
Imports System.Collections.Generic
Imports NimbleAmsBusiness
Imports NimbleAmsLibrary
Imports NimbleAmsLibrary.Business.Customer


' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://leadingage.org/wsvc")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class QualityMetrics
    Inherits System.Web.Services.WebService


    <WebMethod()> _
    Public Function HelloWorld() As String
        Return "Hello World"
    End Function

    <WebMethod()> _
    Public Function Authenticate(ByVal API_Username As String, ByVal API_Password As String, ByVal Username As String, ByVal Password As String) As QMXML
        If CheckAPICredentials(API_Username, API_Password) Then
            Dim ReturnValue As New QMXML()

            If Username.ToLower.Contains("member") Then
                ReturnValue.Status = "Success"
                ReturnValue.FirstName = "John"
                ReturnValue.LastName = "Doe"
                ReturnValue.OrganizationName = "Widgets R Us"
                ReturnValue.State = "DC"
                ReturnValue.LeadingAgeID = "123456"
                ReturnValue.ProviderMemberStatus = True

            ElseIf Username.ToLower.Contains("non") Then
                ReturnValue.Status = "Success"
                ReturnValue.FirstName = "Sally"
                ReturnValue.LastName = "Fields"
                ReturnValue.OrganizationName = "Volunteers of America"
                ReturnValue.State = "PA"
                ReturnValue.LeadingAgeID = "112233"
                ReturnValue.ProviderMemberStatus = False

            ElseIf Username.ToLower.Contains("invalid") Then
                ReturnValue.Status = "Invalid"

            ElseIf Username.ToLower.Contains("doesnotexit") Then
                ReturnValue.Status = "DoesNotExit"
            End If

            Return ReturnValue

        Else
            Return Nothing
        End If

    End Function


    <WebMethod()> _
    Public Function Authenticate_new(ByVal API_Username As String, ByVal API_Password As String, ByVal Username As String, ByVal Password As String) As QMXML
        If CheckAPICredentials(API_Username, API_Password) Then
            Dim ReturnValue As New QMXML()

            Dim myUtility As New CUtility()
            myUtility = GetUtility()

            Dim myAccount As New CAccount

            myAccount = CWebUtility.CheckLogin(Username, Password, myUtility)

            If Not myAccount Is Nothing Then
                ReturnValue.Status = "Success"
                ReturnValue.FirstName = "John"
                ReturnValue.LastName = "Doe"
                ReturnValue.OrganizationName = "Widgets R Us"
                ReturnValue.State = "DC"
                ReturnValue.LeadingAgeID = "123456"
                ReturnValue.ProviderMemberStatus = True
            Else

                ReturnValue.Status = "Invalid"
                ' ReturnValue.Status = "DoesNotExit"
            End If

            Return ReturnValue

        Else
            Return Nothing
        End If

    End Function


    Private Function CheckAPICredentials(ByVal apiUsername As String, ByVal apiPassword As String) As Boolean
        If apiUsername.ToLower() = "webuser" And apiPassword = "w3bus3r_waterloo" Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Function GetUtility() As CUtility
        Dim newUtility As New CUtility()

        newUtility.EnterpriseWrapper = New NimbleAmsSfWrapper.CEnterprise()
        newUtility.EnterpriseWrapper.Password = ConfigurationManager.AppSettings("SfApiPassword")
        newUtility.EnterpriseWrapper.UserName = ConfigurationManager.AppSettings("SfApiUserName")
        newUtility.EnterpriseWrapper.EntityId = ConfigurationManager.AppSettings("Entity")

        Return newUtility

    End Function

End Class

Public Class QMXML
    Public Status As String
    Public FirstName As String
    Public LastName As String
    Public OrganizationName As String
    Public State As String
    Public LeadingAgeID As String
    Public ProviderMemberStatus As Boolean
End Class