Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Security.Cryptography
Imports AAHSA.MDC_Framework.SystemFramework
Imports SalesForceClassLibrary
Imports SalesForceClassLibrary.SalesforceEnterprise
Imports System.Collections.Generic

<WebService(Namespace:="http://leadingage.org/wsvc")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class LaserReg
    Inherits System.Web.Services.WebService

    '<WebMethod()> _
    'Public Function HelloWorld() As String
    '    Return "Hello World"
    'End Function


    '<WebMethod()> _
    'Public Function GetIndividual_OLD(ByVal Individual_ID As String) As String

    '    Dim objIndividual As New AAHSA.MDC_Framework.BusinessObjects.Individual

    '    Dim WebUser As New AAHSA.MDC_Framework.Security.BusinessPrincipal()
    '    WebUser.SharepointAuthenticate("gchowdhry")

    '    objIndividual = objIndividual.GetByID(Individual_ID, WebUser)
    '    Dim fname As String = objIndividual.FirstName

    '    Return fname

    'End Function

    Private KEY As String = "L3@D1NGage"
    Private IVector() As Byte = {27, 9, 45, 27, 0, 72, 171, 54}

    <WebMethod()> _
    Public Function GetIndividual(ByVal IndividualID As String, ByVal ConferenceID As String, ByVal Username As String, ByVal Password As String) As SimpleIndividual       
        Try

            If isAuthenticated(Username, Password) Then
                IndividualID = Server.UrlDecode(IndividualID)
                IndividualID = Decrypt(IndividualID)

                Dim Individuals As List(Of Account)

                Dim strSOQL As String = "SELECT id, firstname, NU__MiddleName__c, lastname, persontitle, NU__PrimaryAffiliation__r.Name, NU__PrimaryAffiliation__r.Id, personemail, shippingstreet, shippingcity, shippingstate, shippingpostalcode,shippingcountry, phone, fax FROM ACCOUNT WHERE ID = '" & IndividualID & "'"

                Dim MyQuerrier As New Querier(Of Account)()
                Individuals = MyQuerrier.Query(strSOQL)

                If Individuals.Count = 1 Then
                    Dim si As New SimpleIndividual()
                    si.IndividualID = Encrypt(IndividualID)
                    si.FirstName = Individuals(0).FirstName
                    si.LastName = Individuals(0).LastName
                    si.MiddleName = Individuals(0).NU__MiddleName__c
                    si.Title = Individuals(0).PersonTitle
                    si.Email = Individuals(0).PersonEmail
                    si.Address1 = Individuals(0).ShippingStreet
                    si.City = Individuals(0).ShippingCity
                    si.State = Individuals(0).ShippingState
                    si.State = "US_" & si.State 'append US_ to value
                    si.Zip = Individuals(0).ShippingPostalCode
                    si.Country = Individuals(0).ShippingCountry
                    If si.Country = "USA" Then
                        si.Country = "US"
                    End If

                    si.Phone = Individuals(0).Phone
                    si.Fax = Individuals(0).Fax

                    If Not Individuals(0).NU__PrimaryAffiliation__r Is Nothing Then
                        si.CompanyName = Individuals(0).NU__PrimaryAffiliation__r.Name
                        si.CompanyID = Individuals(0).NU__PrimaryAffiliation__r.Id
                    End If
                  

                    'Query Reg Object
                    Dim RegData As List(Of NU__Registration2__c)

                    strSOQL = "SELECT id, ShowCare_UniqueID__c, ShowCare_GroupID__c, Registration_Group__r.Id, Registration_Group__r.Group_Owner__c FROM NU__Registration2__c WHERE NU__Account__c = '" & IndividualID & "' AND NU__Event__r.NU__ShortName__c = '" & ConferenceID & "'"
                    Dim RegQuery As New Querier(Of NU__Registration2__c)()
                    RegData = RegQuery.Query(strSOQL)

                    si.ShowCareBadgeID = ""
                    si.isGroupOwner = "0"
                    si.ShowCareGroupID = ""

                    If RegData.Count = 1 Then

                        si.ShowCareBadgeID = RegData(0).ShowCare_UniqueID__c & ""

                        If Not RegData(0).Registration_Group__r Is Nothing Then
                            si.ShowCareGroupID = RegData(0).ShowCare_GroupID__c & ""
                            If RegData(0).Registration_Group__r.Group_Owner__c = IndividualID Then
                                si.isGroupOwner = "1"
                            End If
                        End If
                    End If

                    Return si

                Else
                    Return Nothing

                End If

            End If


            '    Dim objIndividual As New AAHSA.MDC_Framework.BusinessObjects.Individual
            '    Dim WebUser As New AAHSA.MDC_Framework.Security.BusinessPrincipal()
            '    WebUser = Authenticate(Username, Password)

            '    If WebUser.Identity.IsAuthenticated Then
            '        objIndividual = objIndividual.GetByID(IndividualID, WebUser)

            '        Dim si As New SimpleIndividual()
            '        si.IndividualID = Encrypt(IndividualID)
            '        si.FirstName = objIndividual.FirstName
            '        si.LastName = objIndividual.LastName
            '        si.MiddleName = objIndividual.MiddleName
            '        si.Title = objIndividual.Title
            '        si.Email = objIndividual.Email
            '        si.Address1 = objIndividual.IndividualAddresses.MailingAddress.AddrLine1
            '        si.Address2 = objIndividual.IndividualAddresses.MailingAddress.AddrLine2
            '        si.City = objIndividual.IndividualAddresses.MailingAddress.City
            '        si.Country = objIndividual.IndividualAddresses.MailingAddress.Country

            '        'If si.Country = "US" Then                    
            '        '    si.State = "US_" & objIndividual.IndividualAddresses.MailingAddress.StateProvince
            '        'ElseIf si.Country = "CA" Then
            '        '    si.State = "CA_" & objIndividual.IndividualAddresses.MailingAddress.StateProvince
            '        'Else
            '        '    si.State = objIndividual.IndividualAddresses.MailingAddress.StateProvince
            '        'End If
            '        si.State = objIndividual.IndividualAddresses.MailingAddress.StateProvince

            '        si.Zip = objIndividual.IndividualAddresses.MailingAddress.PostalCode

            '        si.Phone = objIndividual.IndividualAddresses.MailingAddress.Phone
            '        si.Fax = objIndividual.IndividualAddresses.MailingAddress.FAX

            '        si.CompanyName = objIndividual.CompanyName ' This may need to be updated to return a company ID?
            '        si.CompanyID = objIndividual.CompanyID

            '        Dim reg As New AAHSA.MDC_Framework.BusinessObjects.Registration
            '        si.ShowCareBadgeID = reg.GetRegVendorBadgeID(IndividualID, ConferenceID) & ""

            '        si.ShowCareGroupID = reg.GetRegVendorGroupID(IndividualID, ConferenceID) & ""

            '        Try
            '            reg = objIndividual.Registrations.FindRegistration(ConferenceID, IndividualID)

            '            If reg.isGroupOwner Then
            '                si.isGroupOwner = "1"
            '            Else
            '                si.isGroupOwner = "0"
            '            End If
            '        Catch ex As Exception
            '            'If group owner, that is not attending conference (this not found in find)
            '            Dim objReg As New AAHSA.MDC_Framework.BusinessObjects.Registration()
            '            Dim GroupID As String
            '            GroupID = Convert.ToString(objReg.GetGroupOwner(IndividualID, ConferenceID))
            '            If GroupID <> "0" Then
            '                si.isGroupOwner = "1"
            '            Else
            '                si.isGroupOwner = "0"
            '            End If

            '        End Try

            '        Return si
            '    Else
            '        Return Nothing
            '    End If


        Catch ex As Exception
            HttpContext.Current.Response.Write(ex.Message)
            Return Nothing
        End Try

    End Function

    <WebMethod()> _
    Public Function EditIndividual(ByVal IndividualID As String, ByVal S_Individual As SimpleIndividual, ByVal Username As String, ByVal Password As String) As String
        Try

            If isAuthenticated(Username, Password) Then
                IndividualID = Server.UrlDecode(IndividualID)
                IndividualID = Decrypt(IndividualID)

                Dim eServe As EnterpriseService = New EnterpriseService()

                Dim Individuals As List(Of Account)
                Dim strSOQL As String = "SELECT id, firstname, NU__MiddleName__c, lastname, persontitle, personemail, billingstreet, billingcity, billingstate, billingpostalcode, billingcountry, shippingstreet, shippingcity, shippingstate, shippingpostalcode, shippingcountry, phone, fax FROM ACCOUNT WHERE ID = '" & IndividualID & "'"

                Dim MyQuerrier As New Querier(Of Account)()
                Individuals = MyQuerrier.Query(strSOQL)

                If Individuals.Count = 1 Then
                    Dim si As New SimpleIndividual()
                    Dim IndividualIsDirty As Boolean = False

                    If Individuals(0).FirstName <> S_Individual.FirstName Then
                        Individuals(0).FirstName = S_Individual.FirstName
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).LastName <> S_Individual.LastName Then
                        Individuals(0).LastName = S_Individual.LastName
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).NU__MiddleName__c <> S_Individual.MiddleName Then
                        Individuals(0).NU__MiddleName__c = S_Individual.MiddleName
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).PersonTitle <> S_Individual.Title Then
                        Individuals(0).PersonTitle = S_Individual.Title
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).Phone <> S_Individual.Phone Then
                        Individuals(0).Phone = S_Individual.Phone
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).Fax <> S_Individual.Fax Then
                        Individuals(0).Fax = S_Individual.Fax
                        IndividualIsDirty = True
                    End If


                    If Individuals(0).PersonEmail <> S_Individual.Email Then
                        Individuals(0).PersonEmail = S_Individual.Email
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).ShippingStreet <> S_Individual.Address1 Then
                        Individuals(0).ShippingStreet = S_Individual.Address1
                        IndividualIsDirty = True
                    End If

                    If Not String.IsNullOrEmpty(S_Individual.Address2) Then
                        Individuals(0).ShippingStreet = Individuals(0).ShippingStreet & vbCrLf & S_Individual.Address2
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).ShippingCity <> S_Individual.City Then
                        Individuals(0).ShippingCity = S_Individual.City
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).ShippingState <> S_Individual.State.Replace("US_", "") Then
                        Individuals(0).ShippingState = S_Individual.State.Replace("US_", "")
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).ShippingPostalCode <> S_Individual.Zip Then
                        Individuals(0).ShippingPostalCode = S_Individual.Zip
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).ShippingCountry <> S_Individual.Country Then
                        Individuals(0).ShippingCountry = S_Individual.Country
                        IndividualIsDirty = True
                    End If

                    If String.IsNullOrEmpty(Individuals(0).BillingStreet) And String.IsNullOrEmpty(Individuals(0).BillingCity) Then
                        Individuals(0).BillingStreet = Individuals(0).ShippingStreet
                        Individuals(0).BillingCity = Individuals(0).ShippingCity
                        Individuals(0).BillingState = Individuals(0).ShippingState
                        Individuals(0).BillingPostalCode = Individuals(0).ShippingPostalCode
                        Individuals(0).BillingCountry = Individuals(0).ShippingCountry
                    End If

                    If IndividualIsDirty Then
                        eServe.upsert(New Account() {Individuals(0)})
                    End If

                    Return True
                End If
            End If

            Return False


        Catch ex As Exception
            Return "False: " & ex.Message & ": Details: " & ex.ToString
        End Try

    End Function


    'This Function marks the beginning of the Vendor Registration Stage
    <WebMethod()> _
    Public Function BeginRegistration(ByVal IndividualID As String, ByVal ConferenceID As String, ByVal IP_Address As String, ByVal Username As String, ByVal Password As String) As Boolean
        Try
            If isAuthenticated(Username, Password) Then
                IndividualID = Server.UrlDecode(IndividualID)
                IndividualID = Decrypt(IndividualID)

                Dim eServe As EnterpriseService = New EnterpriseService()
                Dim MyReg As List(Of NU__Registration2__c) = New List(Of NU__Registration2__c)

                Dim strSOQL As String = "SELECT id, ShowCare_Start__c FROM NU__Registration2__c WHERE NU__Account__c = '" & IndividualID & "' AND NU__Event__r.NU__ShortName__c = '" & ConferenceID & "'"

                Dim MyQuerier As New Querier(Of NU__Registration2__c)
                MyReg = MyQuerier.Query(strSOQL)

                If MyReg.Count = 1 Then
                    If MyReg(0).ShowCare_Start__c Is Nothing Then
                        MyReg(0).ShowCare_Start__c = DateTime.Now.ToUniversalTime().ToString("s")
                        MyReg(0).ShowCare_Start__cSpecified = True
                        eServe.upsert(New NU__Registration2__c() {MyReg(0)})
                    End If
                End If

                Return True
            End If

            Return False

            'Dim objIndividual As New AAHSA.MDC_Framework.BusinessObjects.Individual

            'Dim WebUser As New AAHSA.MDC_Framework.Security.BusinessPrincipal()
            'WebUser = Authenticate(Username, Password)
            'If WebUser.Identity.IsAuthenticated Then
            '    'Create Activity for beginning registration proccess
            '    Dim reg As New AAHSA.MDC_Framework.BusinessObjects.Registration()

            '    'Create stage for start of the vendor step
            '    Dim RegID As Integer = reg.GetRegID(IndividualID, ConferenceID)
            '    reg.MarkStatus(reg.RegStatus.Vender_Start, RegID, IP_Address, Date.Now())
            'End If
            '
            'Return True


        Catch ex As Exception
            Return False
        End Try

    End Function

    'This method marks the completion of the Vendor Registration Stage
    <WebMethod()> _
    Public Function CompleteRegistration(ByVal IndividualID As String, ByVal ShowCareBadgeID As String, ByVal ShowCareGroupID As String, ByVal isGroupOwner As String, ByVal ConferenceID As String, ByVal AmountPaid As Decimal, ByVal BadgeType As String, ByVal IP_Address As String, ByVal Username As String, ByVal Password As String) As Boolean
        Try

            If isAuthenticated(Username, Password) Then
                IndividualID = Server.UrlDecode(IndividualID)
                IndividualID = Decrypt(IndividualID)

                Dim eServe As EnterpriseService = New EnterpriseService()
                Dim MyReg As List(Of NU__Registration2__c) = New List(Of NU__Registration2__c)

                Dim strSOQL As String = "SELECT id, ShowCare_Finish__c, ShowCare_UniqueID__c, ShowCare_AmountPaid__c, ShowCare_BadgeType__c, ShowCare_GroupID__c, ShowCare_LastUpdated__c   FROM NU__Registration2__c WHERE NU__Account__c = '" & IndividualID & "' AND NU__Event__r.NU__ShortName__c = '" & ConferenceID & "'"

                Dim MyQuerier As New Querier(Of NU__Registration2__c)
                MyReg = MyQuerier.Query(strSOQL)

                If MyReg.Count = 1 Then
                    If MyReg(0).ShowCare_Finish__c Is Nothing Then
                        'Initial Insert
                        MyReg(0).ShowCare_Finish__c = DateTime.Now.ToUniversalTime().ToString("s")
                        MyReg(0).ShowCare_Finish__cSpecified = True
                    End If

                    MyReg(0).ShowCare_UniqueID__c = ShowCareBadgeID
                    MyReg(0).ShowCare_AmountPaid__c = AmountPaid
                    MyReg(0).ShowCare_AmountPaid__cSpecified = True
                    MyReg(0).ShowCare_BadgeType__c = BadgeType

                    If Not String.IsNullOrEmpty(ShowCareGroupID) Then
                        MyReg(0).ShowCare_GroupID__c = ShowCareGroupID
                    End If
                    MyReg(0).NU__Status__c = "Active"

                    MyReg(0).ShowCare_LastUpdated__c = DateTime.Now.ToUniversalTime().ToString("s")
                    MyReg(0).ShowCare_LastUpdated__cSpecified = True

                    eServe.upsert(New NU__Registration2__c() {MyReg(0)})

                    Return True
                End If
            End If


            Return False


            'Dim WebUser As New AAHSA.MDC_Framework.Security.BusinessPrincipal()
            'WebUser = Authenticate(Username, Password)

            'IndividualID = Server.UrlDecode(IndividualID)
            'IndividualID = Decrypt(IndividualID)

            'AAHSA.MDC_Framework.SystemFramework.ApplicationLog.WriteLog(WebUser, ApplicationLog.MDC_Log_Categories.INDIVIDUALS, ApplicationLog.MDC_Log_Operations.EDIT, True, "RegWebService - Complete Start: " & IndividualID)


            'Dim objIndividual As New AAHSA.MDC_Framework.BusinessObjects.Individual

            'Dim WebURL As String = System.Web.HttpContext.Current.Request.ServerVariables("server_name")


            'If WebUser.Identity.IsAuthenticated Then

            '    objIndividual = objIndividual.GetByID(IndividualID, WebUser)

            '    Dim reg As New AAHSA.MDC_Framework.BusinessObjects.Registration()

            '    'Create Activity for completed registration
            '    Dim RegID As Integer = reg.GetRegID(IndividualID, ConferenceID)

            '    'Check To See If  A complete Exists
            '    'IF Not -> Mark Status as Complete, SetBagge Id
            '    'IF Exists
            '    'Check to See if Update is Exists
            '    'If Not -> Mark Status As Update
            '    'If Exists -> Update Status Date & IP Address of Update

            '    Dim GroupOwner As Boolean = False

            '    If reg.CheckStatusExists(reg.RegStatus.Vender_End, RegID) = False Then
            '        reg.MarkStatus(reg.RegStatus.Vender_End, RegID, IP_Address, Date.Now())
            '        reg.SetRegBadgeID(IndividualID, ConferenceID, ShowCareBadgeID)

            '        If Not String.IsNullOrEmpty(ShowCareGroupID) Then

            '            'If Not String.IsNullOrEmpty(isGroupOwner) Then
            '            '    If isGroupOwner = "1" Then
            '            '        GroupOwner = True
            '            '    End If
            '            'End If
            '            reg.SetRegVendorGroupID(IndividualID, ConferenceID, ShowCareGroupID)

            '        End If
            '        Dim templateFile As String

            '        If objIndividual.MDCAccount.UserName = "" Then
            '            If Not objIndividual.MDCAccount.IsEmailAddressInSystem(objIndividual.Email) Then
            '                'create user account with random password
            '                'activate account
            '                'send confirmation email

            '                Dim rnd As New Random()
            '                Dim strPassword As String = "leadingage" & rnd.Next(1000, 9999).ToString()
            '                strPassword = objIndividual.MDCAccount.EncryptPassword(strPassword)
            '                objIndividual.MDCAccount = objIndividual.MDCAccount.CreateMDCIndividualAccount(WebUser, objIndividual.Email, strPassword, AAHSA.MDC_Framework.Security.ValidSecurityRoles.GENERAL_USER)
            '                objIndividual.MDCAccount.ActivationKey = ""
            '                objIndividual.MDCAccount.Save(WebUser, IndividualID)

            '                Try
            '                    templateFile = "Files/ConferenceReg_Confirmation_with_login.html"
            '                    AAHSA.MDC_Framework.SystemFramework.Messaging.SendEmailConfirmation(objIndividual.Email, "meeting@LeadingAge.org", "2013 PEAK Leadership Summit and EXPO - Registration Confirmation", "", "", objIndividual.MDCAccount.UserName, strPassword, templateFile, WebURL)
            '                Catch ex As Exception
            '                    'System.Web.HttpContext.Current.Response.Write(ex.Message & "<br/><br/>" & ex.ToString())
            '                    'Return False
            '                    AAHSA.MDC_Framework.SystemFramework.ApplicationLog.WriteLog(WebUser, ApplicationLog.MDC_Log_Categories.INDIVIDUALS, ApplicationLog.MDC_Log_Operations.EDIT, True, "RegWebService - Email New User Error: " & IndividualID & " - " & ex.Message.ToString)
            '                End Try
            '            End If
            '        Else
            '            'send conirmation email
            '            Try
            '                templateFile = "Files/ConferenceReg_Confirmation_without_login.html"
            '                AAHSA.MDC_Framework.SystemFramework.Messaging.SendEmailConfirmation(objIndividual.Email, "meeting@LeadingAge.org", "2013 PEAK Leadership Summit and EXPO - Registration Confirmation ", "", "", "", "", templateFile, WebURL)
            '            Catch ex As Exception
            '                'System.Web.HttpContext.Current.Response.Write(ex.Message & "<br/><br/>" & ex.ToString())
            '                'Return False
            '                AAHSA.MDC_Framework.SystemFramework.ApplicationLog.WriteLog(WebUser, ApplicationLog.MDC_Log_Categories.INDIVIDUALS, ApplicationLog.MDC_Log_Operations.EDIT, True, "RegWebService - Email Exisitng User Error: " & IndividualID & " - " & ex.Message.ToString)
            '            End Try
            '        End If
            '    Else
            '        If reg.CheckStatusExists(reg.RegStatus.Vender_Update, RegID) = False Then
            '            reg.MarkStatus(reg.RegStatus.Vender_Update, RegID, IP_Address, Date.Now())
            '        Else
            '            'Update Update goes here
            '            reg.UpdateUpdateDate(RegID, IP_Address, Date.Now())
            '        End If
            '    End If

            '    'Udpate Amount Paid & Badge Type regardless of actions
            '    reg.UpdateRegData(RegID, AmountPaid, BadgeType)

            'End If

            'Return True

        Catch ex As Exception
           
            Return False
        End Try

    End Function


    'This method marks the cancellation of the Vendor Registration
    <WebMethod()> _
    Public Function CancelRegistration(ByVal IndividualID As String, ConferenceID As String, ByVal IP_Address As String, ByVal Username As String, ByVal Password As String) As Boolean
        Try
            If isAuthenticated(Username, Password) Then
                IndividualID = Server.UrlDecode(IndividualID)
                IndividualID = Decrypt(IndividualID)

                Dim eServe As EnterpriseService = New EnterpriseService()
                Dim MyReg As List(Of NU__Registration2__c) = New List(Of NU__Registration2__c)

                Dim strSOQL As String = "SELECT id, ShowCare_Start__c FROM NU__Registration2__c WHERE NU__Account__c = '" & IndividualID & "' AND NU__Event__r.NU__ShortName__c = '" & ConferenceID & "'"

                Dim MyQuerier As New Querier(Of NU__Registration2__c)
                MyReg = MyQuerier.Query(strSOQL)

                If MyReg.Count = 1 Then
                    MyReg(0).NU__Status__c = "Cancelled"
                    eServe.upsert(New NU__Registration2__c() {MyReg(0)})
                End If


                Return True
            End If

            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function



    Private Function Authenticate(ByVal Username As String, ByVal Password As String) As AAHSA.MDC_Framework.Security.BusinessPrincipal
        Dim WebUser As New AAHSA.MDC_Framework.Security.BusinessPrincipal()
        WebUser.Authenticate(Username, Password)

        Return WebUser

    End Function


    Public Function Encrypt(ByVal str As String) As String
        Dim objEncrypt As New AAHSA.MDC_Framework.SystemFramework.ExternalEncryption
        Dim pKey As String = System.Configuration.ConfigurationManager.AppSettings("pubKey1")
        Dim strEntryptID As String = objEncrypt.Encrypt(str, pKey)

        Return strEntryptID

    End Function

    Public Function Decrypt(ByVal str As String) As String

        Dim objEncrypt As New AAHSA.MDC_Framework.SystemFramework.ExternalEncryption
        Dim strDecriptID As String = objEncrypt.Decrypt(str, System.Configuration.ConfigurationManager.AppSettings("pubKey1"))

        Return strDecriptID

    End Function

    Private Function isAuthenticated(ByVal Username As String, ByVal Password As String) As Boolean
        If Username.ToLower() = "webuser" And Password = "w3bus3r_waterloo" Then
            Return True
        Else
            Return False
        End If
    End Function

    'This Function marks the beginning of the Vendor Registration Stage
    <WebMethod()> _
    Public Function Test(ByVal IndividualID As String) As String
        Try
            IndividualID = Encrypt(IndividualID)
            Return Server.UrlEncode(IndividualID)

        Catch ex As Exception
            Return "exception"
        End Try

    End Function
End Class



Public Class SimpleIndividual
    Public IndividualID As String
    Public FirstName As String
    Public LastName As String
    Public MiddleName As String
    Public Title As String
    Public CompanyName As String
    Public CompanyID As String
    Public Address1 As String
    Public Address2 As String
    Public City As String
    Public State As String
    Public Zip As String
    Public Country As String
    Public Phone As String
    Public Fax As String
    Public Email As String
    Public ShowCareBadgeID As String
    Public ShowCareGroupID As String
    Public isGroupOwner As String
End Class
