
Partial Class Test
    Inherits System.Web.UI.Page



    Protected Sub test_edit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles test_edit.Click

        Try
            Dim ws As New LaserReg()

            Dim si As New SimpleIndividual
            si.FirstName = "Shaun"
            si.LastName = "Hanliner "
            si.Email = "shawn-edit@email.com"
            si.Phone = "610-555-1212"
            si.Fax = "610-555-1313"
            si.Address1 = "1234 My Way Blvd"
            si.City = "City"
            si.State = "PA"
            si.Zip = "12345-6789"
            si.Title = "TesteR"

            Dim ID As String = "EHROfS10KoA%2bSVv0yfZ7aYXeDBMhKo2q"

            'Dim objEncrypt As New AAHSA.MDC_Framework.SystemFramework.ExternalEncryption
            'Dim pKey As String = System.Configuration.ConfigurationManager.AppSettings("pubKey1")
            'Dim strEntryptID As String = objEncrypt.Encrypt(ID, pKey)

            Dim result As String = ""
            result = ws.EditIndividual(ID, si, "webuser", "w3bus3r_waterloo")
            Response.Write(result)
        Catch ex As Exception
            Response.Write(ex.Message & "<br/><br/>" & ex.InnerException.ToString & "<br/><br/>" & ex.ToString)
        End Try

    End Sub


    Protected Sub GENID_Click(sender As Object, e As System.EventArgs) Handles GENID.Click
        Dim objEncrypt As New AAHSA.MDC_Framework.SystemFramework.ExternalEncryption
        Dim pKey As String = System.Configuration.ConfigurationManager.AppSettings("pubKey1")
        Dim strEntryptID As String = objEncrypt.Encrypt(number.Text, pKey)
        strEntryptID = Server.UrlEncode(strEntryptID)
        Response.Write(strEntryptID)
    End Sub

    Protected Sub send_Click(sender As Object, e As System.EventArgs) Handles send.Click
        Dim WebURL As String = System.Web.HttpContext.Current.Request.ServerVariables("server_name")
        Dim templateFile As String = "Files/ConferenceReg_Confirmation_without_login.html"

        Response.Write("START<br/>")

        AAHSA.MDC_Framework.SystemFramework.Messaging.SendEmailConfirmation("gchowdhry@gmail.com", "meeting@LeadingAge.org", "Email Test From", "", "", "", "", templateFile, WebURL)

        Response.Write("END<br/>")

    End Sub
End Class
