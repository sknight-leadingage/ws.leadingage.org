Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Security.Cryptography
Imports SalesForceClassLibrary
Imports SalesForceClassLibrary.SalesforceEnterprise
Imports System.Collections.Generic

<WebService(Namespace:="http://leadingage.org/wsvc")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class LaserReg
    Inherits System.Web.Services.WebService

    Private KEY As String = "L3@D1NGage"
    Private IVector() As Byte = {27, 9, 45, 27, 0, 72, 171, 54}

    Private _lul As String = ""
    Public Property LastUsedLogin As String
        Get
            Return _lul
        End Get
        Set(value As String)
            _lul = value
        End Set
    End Property

    Class AccountTypeAndMemberStatus
        Public Property MemberStatus As String
        Public Property ExtraType As String
        Public Property AccountType As String

        Public Sub New()
            Me.MemberStatus = "Non-Member"
            Me.ExtraType = ""
            Me.AccountType = ""
        End Sub
    End Class

    Friend Function GetStatusFromAccount(ByRef Individual As Account, ByRef eServe As EnterpriseService) As AccountTypeAndMemberStatus
        Dim retVal As New AccountTypeAndMemberStatus()

        If Not Individual.NU__PrimaryAffiliation__r Is Nothing Then
            If Not Individual.NU__PrimaryAffiliation__r.Type Is Nothing AndAlso Not String.IsNullOrEmpty(Individual.NU__PrimaryAffiliation__r.Type) Then
                retVal.ExtraType = Individual.NU__PrimaryAffiliation__r.Type
            End If

            If Not Individual.NU__PrimaryAffiliation__r.NU__RecordTypeName__c Is Nothing AndAlso Not String.IsNullOrEmpty(Individual.NU__PrimaryAffiliation__r.NU__RecordTypeName__c) Then
                retVal.AccountType = Individual.NU__PrimaryAffiliation__r.NU__RecordTypeName__c
                If Not Individual.NU__PrimaryAffiliation__r.Provider_Member__c Is Nothing AndAlso Individual.NU__PrimaryAffiliation__r.Provider_Member__c.Equals("Yes", StringComparison.CurrentCultureIgnoreCase) Then
                    retVal.MemberStatus = "Member"
                ElseIf Not Individual.NU__PrimaryAffiliation__r.NU__Member__c Is Nothing AndAlso Individual.NU__PrimaryAffiliation__r.NU__Member__c.Equals("Yes", StringComparison.CurrentCultureIgnoreCase) Then
                    retVal.MemberStatus = "Member"
                ElseIf Not Individual.NU__PrimaryAffiliation__r.CAST_Member__c Is Nothing AndAlso Individual.NU__PrimaryAffiliation__r.CAST_Member__c.Equals("Yes", StringComparison.CurrentCultureIgnoreCase) Then
                    retVal.MemberStatus = "Member"
                ElseIf Not Individual.NU__PrimaryAffiliation__r.IAHSA_Member__c Is Nothing AndAlso Individual.NU__PrimaryAffiliation__r.IAHSA_Member__c.Equals("Yes", StringComparison.CurrentCultureIgnoreCase) Then
                    retVal.AccountType = "Provider"
                    retVal.MemberStatus = "Member"
                ElseIf Not Individual.IAHSA_Member__c Is Nothing AndAlso Individual.IAHSA_Member__c.Equals("Yes", StringComparison.CurrentCultureIgnoreCase) Then
                    retVal.AccountType = "Provider"
                    retVal.MemberStatus = "Member"
                End If
            End If

            If Not Individual.NU__PrimaryAffiliation__r.NU__RecordTypeName__c Is Nothing AndAlso Not String.IsNullOrEmpty(Individual.NU__PrimaryAffiliation__r.NU__RecordTypeName__c) AndAlso Individual.NU__PrimaryAffiliation__r.NU__RecordTypeName__c.Equals("State Partner", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.MemberStatus = "Member"
            End If
        Else
            If Not Individual.Type Is Nothing AndAlso Not String.IsNullOrEmpty(Individual.Type) Then
                retVal.ExtraType = Individual.Type
            End If

            If Not Individual.NU__RecordTypeName__c Is Nothing AndAlso Not String.IsNullOrEmpty(Individual.NU__RecordTypeName__c) Then
                retVal.AccountType = Individual.NU__RecordTypeName__c
                If Not Individual.Provider_Member__c Is Nothing AndAlso Individual.Provider_Member__c.Equals("Yes", StringComparison.CurrentCultureIgnoreCase) Then
                    retVal.MemberStatus = "Member"
                ElseIf Not Individual.NU__Member__c Is Nothing AndAlso Individual.NU__Member__c.Equals("Yes", StringComparison.CurrentCultureIgnoreCase) Then
                    retVal.MemberStatus = "Member"
                ElseIf Not Individual.CAST_Member__c Is Nothing AndAlso Individual.CAST_Member__c.Equals("Yes", StringComparison.CurrentCultureIgnoreCase) Then
                    retVal.MemberStatus = "Member"
                ElseIf Not Individual.IAHSA_Member__c Is Nothing AndAlso Individual.IAHSA_Member__c.Equals("Yes", StringComparison.CurrentCultureIgnoreCase) Then
                    retVal.AccountType = "Provider"
                    retVal.MemberStatus = "Member"
                End If
            End If
        End If

        If Not retVal.ExtraType Is Nothing AndAlso Not String.IsNullOrEmpty(retVal.ExtraType) Then
            If retVal.ExtraType.Equals("Government/Legislative", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Provider"
                retVal.MemberStatus = "Member"
            ElseIf retVal.ExtraType.Equals("Healthcare", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Company"
            ElseIf retVal.ExtraType.Equals("Managed Care Organization", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Provider"
                retVal.MemberStatus = "Member"
            ElseIf retVal.ExtraType.Equals("Management Company", StringComparison.CurrentCultureIgnoreCase) Then
                If retVal.AccountType.Equals("Multi-site Organization", StringComparison.CurrentCultureIgnoreCase) Then
                    retVal.AccountType = "Provider"
                End If
            ElseIf retVal.ExtraType.Equals("Organization", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Provider"
                retVal.MemberStatus = "Member"
            ElseIf retVal.ExtraType.Equals("Other", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Provider"
                retVal.MemberStatus = "Member"
            ElseIf retVal.ExtraType.Equals("University/College", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Provider"
                retVal.MemberStatus = "Member"
            ElseIf retVal.ExtraType.Equals("Association", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Provider"
                retVal.MemberStatus = "Member"
            ElseIf retVal.ExtraType.Equals("Corporate/Business Firm", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Company"
            ElseIf retVal.ExtraType.Equals("Press", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Company"
                retVal.MemberStatus = "Member"
            ElseIf retVal.ExtraType.Equals("NGO", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Provider"
                retVal.MemberStatus = "Member"
            ElseIf retVal.ExtraType.Equals("Law Firm", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Company"
            ElseIf retVal.ExtraType.Equals("Religious", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Provider"
                retVal.MemberStatus = "Member"
            ElseIf retVal.ExtraType.Equals("LeadingAge Vendor", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Company"
            ElseIf retVal.ExtraType.Equals("Educator", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Individual"
            ElseIf retVal.ExtraType.Equals("Student", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Individual"
            ElseIf retVal.ExtraType.Equals("Retired Administrator", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Individual"
            ElseIf retVal.ExtraType.Equals("Resident", StringComparison.CurrentCultureIgnoreCase) Then
                retVal.AccountType = "Individual"
            End If
        End If

        If Not Individual.NU__RecordTypeName__c Is Nothing AndAlso Not String.IsNullOrEmpty(Individual.NU__RecordTypeName__c) _
            AndAlso Individual.NU__RecordTypeName__c.Equals("Individual Associate", StringComparison.CurrentCultureIgnoreCase) _
            AndAlso retVal.MemberStatus.Equals("Non-Member", StringComparison.CurrentCultureIgnoreCase) _
            AndAlso Not Individual.LeadingAge_Member_Individual__c Is Nothing AndAlso Not String.IsNullOrEmpty(Individual.LeadingAge_Member_Individual__c) Then
            retVal.MemberStatus = Individual.LeadingAge_Member_Individual__c
        End If

        If retVal.MemberStatus.Equals("Non-Member", StringComparison.CurrentCultureIgnoreCase) AndAlso Not Individual.NU__PersonContact__c Is Nothing AndAlso Not String.IsNullOrEmpty(Individual.NU__PersonContact__c) Then
            Dim qrtInteractions As New Querier(Of Task)(eServe)
            Dim lInteractions As New List(Of Task)

            Dim strSOQL As String = String.Format("SELECT Id,CreatedDate,Status,Subject,WhoId FROM Task WHERE WhoId = '{0}' AND Subject LIKE '%My.LeadingAge Registration Member Status%' AND IsClosed = false AND Status != 'Completed' AND CreatedDate = LAST_N_DAYS:21", Individual.NU__PersonContact__c)
            lInteractions = qrtInteractions.Query(strSOQL)

            If (lInteractions.Count > 0) Then
                'If there's an interaction that matches the SOQL query criteria, then they've previously checked the box
                retVal.MemberStatus = "Member"
            End If
        End If

        Return retVal
    End Function

    <WebMethod()> _
    Public Function GetIndividual(ByVal IndividualID As String, ByVal ConferenceID As String, ByVal Username As String, ByVal Password As String) As SimpleIndividual
        Try

            If IsWebServiceCallAuthenticated(Username, Password) Then
                IndividualID = Server.UrlDecode(IndividualID)
                IndividualID = Decrypt(IndividualID)

                Dim Individuals As List(Of Account)

                Dim strSOQL As String = String.Format("SELECT id, Type, NU__PersonContact__c, firstname, NU__MiddleName__c, lastname, persontitle, Operational_Level__pc, Organizational_Function_Expertise__pc, NU__PrimaryAffiliation__r.Name, NU__PrimaryAffiliation__r.Id, personemail, shippingstreet, shippingcity, shippingstate, shippingpostalcode,shippingcountry, phone, fax, NU__PrimaryAffiliation__r.Type, NU__PrimaryAffiliation__r.NU__RecordTypeName__c, NU__PrimaryAffiliation__r.Provider_Member__c, NU__PrimaryAffiliation__r.NU__Member__c, NU__PrimaryAffiliation__r.CAST_Member__c, NU__PrimaryAffiliation__r.IAHSA_Member__c, NU__RecordTypeName__c, Provider_Member__c, NU__Member__c, CAST_Member__c, IAHSA_Member__c, LeadingAge_Member_Individual__c FROM ACCOUNT WHERE ID = '{0}'", IndividualID)

                Dim eServe As New EnterpriseService()
                LastUsedLogin = eServe.UsernameInUse

                Dim MyQuerrier As New Querier(Of Account)(eServe)
                Individuals = MyQuerrier.Query(strSOQL)

                If Individuals.Count = 1 Then
                    Dim si As New SimpleIndividual()
                    si.IndividualID = Encrypt(IndividualID)
                    si.FirstName = Individuals(0).FirstName
                    si.LastName = Individuals(0).LastName
                    si.MiddleName = Individuals(0).NU__MiddleName__c
                    si.Title = Individuals(0).PersonTitle
                    si.Email = Individuals(0).PersonEmail
                    si.Address1 = Individuals(0).ShippingStreet
                    si.City = Individuals(0).ShippingCity
                    si.State = Individuals(0).ShippingState
                    si.State = "US_" & si.State 'append US_ to value
                    si.Zip = Individuals(0).ShippingPostalCode
                    si.Country = Individuals(0).ShippingCountry
                    If si.Country = "USA" Then
                        si.Country = "US"
                    End If

                    si.Phone = Individuals(0).Phone
                    si.Fax = Individuals(0).Fax

                    If Not Individuals(0).NU__PrimaryAffiliation__r Is Nothing Then
                        si.CompanyName = Individuals(0).NU__PrimaryAffiliation__r.Name
                        si.CompanyID = Individuals(0).NU__PrimaryAffiliation__r.Id
                    End If

                    si.OperationalLevel = Individuals(0).Operational_Level__pc
                    si.FunctionExpertise = Individuals(0).Organizational_Function_Expertise__pc

                    Dim accStatusTypes As AccountTypeAndMemberStatus = GetStatusFromAccount(Individuals(0), eServe)

                    'Query Reg Object
                    Dim RegData As List(Of NU__Registration2__c)

                    strSOQL = String.Format("SELECT id, ShowCare_UniqueID__c, ShowCare_GroupID__c, Registration_Group__r.Id, Registration_Group__r.Group_Owner__c FROM NU__Registration2__c WHERE NU__Account__c = '{0}' AND NU__Event__r.NU__ShortName__c = '{1}'", IndividualID, ConferenceID)
                    If accStatusTypes.MemberStatus.Equals("Non-Member", StringComparison.CurrentCultureIgnoreCase) Then
                        strSOQL = String.Format("SELECT Id, ShowCare_UniqueID__c, ShowCare_GroupID__c, Registration_Group__r.Id, Registration_Group__r.Group_Owner__c, Registration_Group__r.Group_Owner__r.NU__PersonContact__c, Registration_Group__r.Group_Owner__r.Type, Registration_Group__r.Group_Owner__r.NU__RecordTypeName__c, Registration_Group__r.Group_Owner__r.Provider_Member__c, Registration_Group__r.Group_Owner__r.NU__Member__c, Registration_Group__r.Group_Owner__r.CAST_Member__c, Registration_Group__r.Group_Owner__r.IAHSA_Member__c, Registration_Group__r.Group_Owner__r.NU__PrimaryAffiliation__c, Registration_Group__r.Group_Owner__r.NU__PrimaryAffiliation__r.Type, Registration_Group__r.Group_Owner__r.NU__PrimaryAffiliation__r.NU__RecordTypeName__c, Registration_Group__r.Group_Owner__r.NU__PrimaryAffiliation__r.Provider_Member__c, Registration_Group__r.Group_Owner__r.NU__PrimaryAffiliation__r.NU__Member__c, Registration_Group__r.Group_Owner__r.NU__PrimaryAffiliation__r.CAST_Member__c, Registration_Group__r.Group_Owner__r.NU__PrimaryAffiliation__r.IAHSA_Member__c FROM NU__Registration2__c WHERE NU__Account__c = '{0}' AND NU__Event__r.NU__ShortName__c = '{1}'", IndividualID, ConferenceID)
                    End If
                    Dim RegQuery As New Querier(Of NU__Registration2__c)(eServe)
                    RegData = RegQuery.Query(strSOQL)

                    si.ShowCareBadgeID = ""
                    si.isGroupOwner = "0"
                    si.ShowCareGroupID = ""

                    If RegData.Count = 1 Then

                        si.ShowCareBadgeID = RegData(0).ShowCare_UniqueID__c & ""

                        If Not RegData(0).Registration_Group__r Is Nothing Then
                            si.ShowCareGroupID = RegData(0).ShowCare_GroupID__c & ""
                            If Not RegData(0).Registration_Group__r.Group_Owner__c Is Nothing AndAlso RegData(0).Registration_Group__r.Group_Owner__c = IndividualID Then
                                si.isGroupOwner = "1"
                            End If

                            If accStatusTypes.MemberStatus.Equals("Non-Member", StringComparison.CurrentCultureIgnoreCase) AndAlso Not RegData(0).Registration_Group__r.Group_Owner__r Is Nothing Then
                                Dim accGroupStatus As AccountTypeAndMemberStatus = GetStatusFromAccount(RegData(0).Registration_Group__r.Group_Owner__r, eServe)
                                If accGroupStatus.MemberStatus.Equals("Member", StringComparison.CurrentCultureIgnoreCase) Then
                                    accStatusTypes.AccountType = accGroupStatus.AccountType
                                    accStatusTypes.MemberStatus = accGroupStatus.MemberStatus
                                End If
                            End If
                        End If
                    End If

                    'This code sets the status type to Member/Non-Member (Removes the Account Record Type portion of the status)
                    '   But only for the most recent PEAK event, e.g. Annual Meeting uses the detail
                    '   Implemented for PEAK2016
                    If System.Configuration.ConfigurationManager.AppSettings("MostRecentPEAKID").Equals(ConferenceID) Or _
                         ConferenceID.StartsWith(System.Configuration.ConfigurationManager.AppSettings("MostRecentPEAKID")) _
                         Or ConferenceID.Equals(System.Configuration.ConfigurationManager.AppSettings("MostRecentPEAKCode")) Then
                        si.AccountTypeStatus = accStatusTypes.MemberStatus
                    Else
                        si.AccountTypeStatus = String.Format("{0} - {1}", accStatusTypes.AccountType, accStatusTypes.MemberStatus)
                    End If

                    Return si

                Else
                    Return Nothing
                End If

            End If

            'If we make it to this point then we should return nothing as we would have returned somethig by now if things were working
            Return Nothing
        Catch ex As Exception
            HttpContext.Current.Response.Write(ex.Message)
            Return Nothing
        End Try

    End Function

    <WebMethod()> _
    Public Function EditIndividual(ByVal IndividualID As String, ByVal S_Individual As SimpleIndividual, ByVal Username As String, ByVal Password As String) As String
        Try

            If IsWebServiceCallAuthenticated(Username, Password) Then
                IndividualID = Server.UrlDecode(IndividualID)
                IndividualID = Decrypt(IndividualID)

                Dim eServe As EnterpriseService = New EnterpriseService()

                Dim Individuals As List(Of Account)
                Dim strSOQL As String = "SELECT id, firstname, NU__MiddleName__c, lastname, persontitle, personemail, billingstreet, billingcity, billingstate, billingpostalcode, billingcountry, shippingstreet, shippingcity, shippingstate, shippingpostalcode, shippingcountry, phone, fax FROM ACCOUNT WHERE ID = '" & IndividualID & "'"

                Dim MyQuerrier As New Querier(Of Account)(eServe)
                Individuals = MyQuerrier.Query(strSOQL)

                If Individuals.Count = 1 Then
                    Dim IndividualIsDirty As Boolean = False

                    If Individuals(0).FirstName <> S_Individual.FirstName Then
                        Individuals(0).FirstName = S_Individual.FirstName
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).LastName <> S_Individual.LastName Then
                        Individuals(0).LastName = S_Individual.LastName
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).NU__MiddleName__c <> S_Individual.MiddleName Then
                        Individuals(0).NU__MiddleName__c = S_Individual.MiddleName
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).PersonTitle <> S_Individual.Title Then
                        Individuals(0).PersonTitle = S_Individual.Title
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).Phone <> S_Individual.Phone Then
                        Individuals(0).Phone = S_Individual.Phone
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).Fax <> S_Individual.Fax Then
                        Individuals(0).Fax = S_Individual.Fax
                        IndividualIsDirty = True
                    End If


                    If Individuals(0).PersonEmail <> S_Individual.Email Then
                        Individuals(0).PersonEmail = S_Individual.Email
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).ShippingStreet <> S_Individual.Address1 Then
                        Individuals(0).ShippingStreet = S_Individual.Address1
                        IndividualIsDirty = True
                    End If

                    If Not String.IsNullOrEmpty(S_Individual.Address2) Then
                        Individuals(0).ShippingStreet = Individuals(0).ShippingStreet & vbCrLf & S_Individual.Address2
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).ShippingCity <> S_Individual.City Then
                        Individuals(0).ShippingCity = S_Individual.City
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).ShippingState <> S_Individual.State.Replace("US_", "") Then
                        Individuals(0).ShippingState = S_Individual.State.Replace("US_", "")
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).ShippingPostalCode <> S_Individual.Zip Then
                        Individuals(0).ShippingPostalCode = S_Individual.Zip
                        IndividualIsDirty = True
                    End If

                    If Individuals(0).ShippingCountry <> S_Individual.Country Then
                        Individuals(0).ShippingCountry = S_Individual.Country
                        IndividualIsDirty = True
                    End If

                    If Not String.IsNullOrEmpty(S_Individual.OperationalLevel) AndAlso Individuals(0).Operational_Level__pc <> S_Individual.OperationalLevel Then
                        Individuals(0).Operational_Level__pc = S_Individual.OperationalLevel
                        IndividualIsDirty = True
                    End If

                    If Not String.IsNullOrEmpty(S_Individual.FunctionExpertise) AndAlso Individuals(0).Organizational_Function_Expertise__pc <> S_Individual.FunctionExpertise Then
                        Individuals(0).Organizational_Function_Expertise__pc = S_Individual.FunctionExpertise
                        IndividualIsDirty = True
                    End If

                    If String.IsNullOrEmpty(Individuals(0).BillingStreet) And String.IsNullOrEmpty(Individuals(0).BillingCity) Then
                        Individuals(0).BillingStreet = Individuals(0).ShippingStreet
                        Individuals(0).BillingCity = Individuals(0).ShippingCity
                        Individuals(0).BillingState = Individuals(0).ShippingState
                        Individuals(0).BillingPostalCode = Individuals(0).ShippingPostalCode
                        Individuals(0).BillingCountry = Individuals(0).ShippingCountry
                    End If

                    If IndividualIsDirty Then
                        eServe.upsert(New Account() {Individuals(0)})
                    End If

                    Return True
                End If
            End If

            Return False


        Catch ex As Exception
            Return "False: " & ex.Message & ": Details: " & ex.ToString
        End Try

    End Function


    'This Function marks the beginning of the Vendor Registration Stage
    <WebMethod()> _
    Public Function BeginRegistration(ByVal IndividualID As String, ByVal ConferenceID As String, ByVal IP_Address As String, ByVal Username As String, ByVal Password As String) As Boolean
        Try
            If IsWebServiceCallAuthenticated(Username, Password) Then
                IndividualID = Server.UrlDecode(IndividualID)
                IndividualID = Decrypt(IndividualID)

                Dim eServe As EnterpriseService = New EnterpriseService()
                Dim MyReg As List(Of NU__Registration2__c) = New List(Of NU__Registration2__c)

                Dim strSOQL As String = "SELECT id, ShowCare_Start__c FROM NU__Registration2__c WHERE NU__Account__c = '" & IndividualID & "' AND NU__Event__r.NU__ShortName__c = '" & ConferenceID & "'"

                Dim MyQuerier As New Querier(Of NU__Registration2__c)(eServe)
                MyReg = MyQuerier.Query(strSOQL)

                If MyReg.Count = 1 Then
                    If MyReg(0).ShowCare_Start__c Is Nothing Then
                        MyReg(0).ShowCare_Start__c = DateTime.Now.ToUniversalTime().ToString("s")
                        MyReg(0).ShowCare_Start__cSpecified = True
                        eServe.upsert(New NU__Registration2__c() {MyReg(0)})
                    End If
                End If

                Return True
            End If

            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function

    'This method marks the completion of the Vendor Registration Stage
    <WebMethod()> _
    Public Function CompleteRegistration(ByVal IndividualID As String, ByVal ShowCareBadgeID As String, ByVal ShowCareGroupID As String, ByVal isGroupOwner As String, ByVal ConferenceID As String, ByVal AmountPaid As Decimal, ByVal BadgeType As String, ByVal OperationalLevel As String, ByVal FunctionExpertise As String, ByVal IP_Address As String, ByVal Username As String, ByVal Password As String) As Boolean
        Try

            If IsWebServiceCallAuthenticated(Username, Password) Then
                IndividualID = Server.UrlDecode(IndividualID)
                IndividualID = Decrypt(IndividualID)

                Dim eServe As EnterpriseService = New EnterpriseService()
                Dim MyReg As List(Of NU__Registration2__c) = New List(Of NU__Registration2__c)

                Dim strSOQL As String = "SELECT id, ShowCare_Finish__c, ShowCare_UniqueID__c, ShowCare_AmountPaid__c, ShowCare_BadgeType__c, ShowCare_GroupID__c, ShowCare_LastUpdated__c, NU__Account__r.Operational_Level__pc, NU__Account__r.Organizational_Function_Expertise__pc FROM NU__Registration2__c WHERE NU__Account__c = '" & IndividualID & "' AND NU__Event__r.NU__ShortName__c = '" & ConferenceID & "'"

                Dim MyQuerier As New Querier(Of NU__Registration2__c)(eServe)
                MyReg = MyQuerier.Query(strSOQL)

                If MyReg.Count = 1 Then
                    Dim regSave As New NU__Registration2__c
                    regSave.Id = MyReg(0).Id

                    If MyReg(0).ShowCare_Finish__c Is Nothing Then
                        'Initial Insert
                        MyReg(0).ShowCare_Finish__c = DateTime.Now.ToUniversalTime().ToString("s")
                        MyReg(0).ShowCare_Finish__cSpecified = True

                        regSave.ShowCare_Finish__c = MyReg(0).ShowCare_Finish__c
                        regSave.ShowCare_Finish__cSpecified = True
                    End If

                    MyReg(0).ShowCare_UniqueID__c = ShowCareBadgeID
                    MyReg(0).ShowCare_AmountPaid__c = AmountPaid
                    MyReg(0).ShowCare_AmountPaid__cSpecified = True
                    MyReg(0).ShowCare_BadgeType__c = BadgeType

                    regSave.ShowCare_UniqueID__c = MyReg(0).ShowCare_UniqueID__c
                    regSave.ShowCare_AmountPaid__c = MyReg(0).ShowCare_AmountPaid__c
                    regSave.ShowCare_AmountPaid__cSpecified = True
                    regSave.ShowCare_BadgeType__c = MyReg(0).ShowCare_BadgeType__c

                    If Not String.IsNullOrEmpty(ShowCareGroupID) Then
                        MyReg(0).ShowCare_GroupID__c = ShowCareGroupID
                        regSave.ShowCare_GroupID__c = MyReg(0).ShowCare_GroupID__c
                    End If

                    MyReg(0).NU__Status__c = "Active"
                    regSave.NU__Status__c = MyReg(0).NU__Status__c

                    MyReg(0).ShowCare_LastUpdated__c = DateTime.Now.ToUniversalTime().ToString("s")
                    MyReg(0).ShowCare_LastUpdated__cSpecified = True

                    regSave.ShowCare_LastUpdated__c = MyReg(0).ShowCare_LastUpdated__c
                    regSave.ShowCare_LastUpdated__cSpecified = True

                    eServe.upsert(New NU__Registration2__c() {regSave})

                    Try
                        Dim updateExpertise As New Account()
                        Dim bShouldUpdate As Boolean = False
                        updateExpertise.Id = IndividualID
                        If Not String.IsNullOrEmpty(OperationalLevel) AndAlso updateExpertise.Operational_Level__pc <> OperationalLevel Then
                            updateExpertise.Operational_Level__pc = OperationalLevel
                            bShouldUpdate = True
                        End If
                        If Not String.IsNullOrEmpty(FunctionExpertise) AndAlso updateExpertise.Organizational_Function_Expertise__pc <> FunctionExpertise Then
                            Dim lstrIn As New List(Of String)
                            Dim lstrOut As New List(Of String)
                            Dim strOut As String
                            If Not MyReg(0).NU__Account__r Is Nothing AndAlso Not MyReg(0).NU__Account__r.Organizational_Function_Expertise__pc Is Nothing Then
                                lstrIn.AddRange(MyReg(0).NU__Account__r.Organizational_Function_Expertise__pc.Split(";".ToCharArray()))
                                lstrIn.AddRange(FunctionExpertise.Split(";".ToCharArray()))
                                Dim strCmp As String
                                For Each strItem As String In lstrIn
                                    strCmp = strItem.ToLower().Trim()
                                    If Not lstrOut.Contains(strCmp) Then
                                        lstrOut.Add(strCmp)
                                        If String.IsNullOrEmpty(strOut) Then
                                            strOut = strItem.Trim()
                                        Else
                                            strOut = strOut & "; " & strItem.Trim()
                                        End If
                                    End If
                                Next
                                If lstrOut.Count > 0 AndAlso Not String.IsNullOrEmpty(strOut) Then
                                    FunctionExpertise = strOut
                                End If
                            End If
                            updateExpertise.Organizational_Function_Expertise__pc = FunctionExpertise
                            bShouldUpdate = True
                        End If
                        If bShouldUpdate = True Then
                            eServe.upsert(New Account() {updateExpertise})
                        End If
                    Catch ex As Exception
                    End Try

                    Return True
                End If
            End If

        Return False
        Catch ex As Exception

            Return False
        End Try

    End Function


    'This method marks the cancellation of the Vendor Registration
    <WebMethod()> _
    Public Function CancelRegistration(ByVal IndividualID As String, ConferenceID As String, ByVal IP_Address As String, ByVal Username As String, ByVal Password As String) As Boolean
        Try
            If IsWebServiceCallAuthenticated(Username, Password) Then
                IndividualID = Server.UrlDecode(IndividualID)
                IndividualID = Decrypt(IndividualID)

                Dim eServe As EnterpriseService = New EnterpriseService()
                Dim MyReg As List(Of NU__Registration2__c) = New List(Of NU__Registration2__c)

                Dim strSOQL As String = "SELECT id, ShowCare_Start__c FROM NU__Registration2__c WHERE NU__Account__c = '" & IndividualID & "' AND NU__Event__r.NU__ShortName__c = '" & ConferenceID & "'"

                Dim MyQuerier As New Querier(Of NU__Registration2__c)(eServe)
                MyReg = MyQuerier.Query(strSOQL)

                If MyReg.Count = 1 Then
                    MyReg(0).NU__Status__c = "Cancelled"
                    eServe.upsert(New NU__Registration2__c() {MyReg(0)})
                End If


                Return True
            End If

            Return False
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function Encrypt(ByVal str As String) As String
        Dim objEncrypt As New SalesForceClassLibrary.LeadingAgeEncryption
        Dim pKey As String = System.Configuration.ConfigurationManager.AppSettings("pubKey1")
        Dim strEntryptID As String = objEncrypt.Encrypt(str, pKey)

        Return strEntryptID
    End Function

    Public Function Decrypt(ByVal str As String) As String
        Dim objEncrypt As New SalesForceClassLibrary.LeadingAgeEncryption
        Dim strDecriptID As String = objEncrypt.Decrypt(str, System.Configuration.ConfigurationManager.AppSettings("pubKey1"))

        Return strDecriptID
    End Function

    Private Function IsWebServiceCallAuthenticated(ByVal Username As String, ByVal Password As String) As Boolean
        If Username.Equals(System.Configuration.ConfigurationManager.AppSettings("LeadingAge_ApiUser"), StringComparison.CurrentCultureIgnoreCase) AndAlso Password.Equals(System.Configuration.ConfigurationManager.AppSettings("LeadingAge_ApiPass"), StringComparison.CurrentCultureIgnoreCase) Then
            Return True
        Else
            Return False
        End If
    End Function

    <WebMethod()> _
    Public Function Test(ByVal IndividualID As String) As String
        Try
            IndividualID = Encrypt(IndividualID)
            Return Server.UrlEncode(IndividualID)

        Catch ex As Exception
            Return "exception"
        End Try

    End Function
End Class



Public Class SimpleIndividual
    Public IndividualID As String
    Public FirstName As String
    Public LastName As String
    Public MiddleName As String
    Public Title As String
    Public CompanyName As String
    Public CompanyID As String
    Public Address1 As String
    Public Address2 As String
    Public City As String
    Public State As String
    Public Zip As String
    Public Country As String
    Public Phone As String
    Public Fax As String
    Public Email As String
    Public ShowCareBadgeID As String
    Public ShowCareGroupID As String
    Public isGroupOwner As String
    Public AccountTypeStatus As String
    Public OperationalLevel As String
    Public FunctionExpertise As String
End Class
